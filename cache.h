#pragma once

uint LOADINT( uint* address );
void STOREINT( uint* address, uint value );
float LOADFLOAT( float* a );
void STOREFLOAT( float* a, float f );
vec3 LOADVEC( vec3* a );
void STOREVEC( vec3* a, vec3 t );
void* LOADPTR( void* a );
void STOREPTR( void* a, void* p );

#define STATISTICS		true // Set to true for visualisation, and false for no visualisation.
uint GETMISS();
uint GETHITS();
uint GETACCESS();
Surface* GETGRAPH();
bool GETGRAPHFLAG();
void SETGRAPHFLAG(bool flag);
uint* GETACCESSTIME();
uint GETCURRACCESSTIME();
uint GETTICKACCESSTIME();
uint GETTICKACCESS();
void RESETTICK();

#define CACHESIZE		16 // How many bytes per line in cache.
#define CACHELEVELS		3 // How many levels of cache.

enum POLICY // Enum for eviction policies.
{
	LRU = 1, 
	MRU = 2, 
	RANDOM = 3, 
	LFU = 4
};

#define DELAY dummy = min( 10, dummy + sinf( (float)(address / 1789) ) ); // Artificial delay

struct CacheLine
{
	uint tag; //32-bit
	uint data[CACHESIZE] = { 0 };
	bool valid, dirty;

	// LRU and MRU.
	long int offset; // Offset from the cache age, used to determine the age of the line.

	// LFU
	unsigned long int hits; // Increments by 1 every time this specific cache line is used.

	// Initialise all cache lines.
	CacheLine() : valid(false), dirty(false), offset(0), hits(0), tag(0) {}
};

struct CacheLevel
{
	uint rows, sets; // Each row is comprised of a set of cache lines.
	std::vector<std::vector<CacheLine>> cache = {};

	CacheLine* GetCacheLine(uint row, uint set) { return &cache[row][set]; }

	std::vector<CacheLine*> GetCacheRow(uint row)
	{
		std::vector<CacheLine*> result = {};
		for (CacheLine& cl : cache[row]) { result.push_back(&cl); }
		return result;
	}

	CacheLevel(uint rows_, uint sets_) : rows(rows_), sets(sets_) { cache.resize(rows, std::vector<CacheLine>(sets)); }
};

class Cache
{	
public:
	// Usage parameters.
	uint cache_miss; // Total misses.
	uint cache_hits; // Total hits.
	uint cache_age; // Total accesses.
	uint policy = POLICY::LRU; // Eviction policy.

	// Statistics
	Surface* graph;
	void UpdateGraph();
	bool graphFlag;

	uint access_time[CACHELEVELS + 1] = { 0 };
	uint curr_access_time;
	uint tick_access_time, tick_access, tick_peak;
	unsigned long long access_start, access_end;
	uint count;

	Cache();
	~Cache();
	void ArtificialDelay();

	// Generic Read/Write functions.
	template<typename T> T ReadNbit(uint address);
	template<typename T> void WriteNbit(uint address, T value);

	uint Read32bit(uint address);
	void Write32bit(uint address, uint value);

	float GetDummyValue() const { return dummy; }
private:
	CacheLevel* cache_L1 = new CacheLevel(128, 4); // 4-way set associative with 128 lines = 32KB cache.
	CacheLevel* cache_L2 = new CacheLevel(512, 8); // 8-way set associative with 512 lines = 256KB cache.
	CacheLevel* cache_L3 = new CacheLevel(2048, 16); // 16-way set associative with 2048 lines = 2MB cache.
	CacheLevel* full_cache[CACHELEVELS] = { cache_L1, cache_L2, cache_L3 };

	bool LoadLineFromCache(uint address, CacheLine& line); // Loads a line from cache and stores it in the line parameter. Returns true if it found the line and false otherwise.
	void WriteLineToCache(uint address, CacheLine& line); // Writes a given line with the given address to cache if it finds a spot. Calls Evict(address, line) if it doesn't find a spot.
	void UpdateLineInCache(uint address, CacheLine& line); // Updates the line with the given address in the cache to the new data.
	void Evict(uint address, CacheLine line); // This is called when the cache is full. It makes space for the new data by evicting each line being replaced to the next level of cache (or memory if the line is in the last level of cache).
	void ResetLine(CacheLine& line); // Resets a line to its default state.
	void ResetOffsets(); // Resets the age offset value in each cache line.
	void LoadLineFromMem(uint address, CacheLine& line); // Loads a line with the given address from memory.
	void WriteLineToMem(uint address, CacheLine& line); // Writes a line with the given address to memory.
	float dummy;
};
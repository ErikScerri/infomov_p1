#include	"precomp.h"

// Instantiate the cache
Cache cache;

// Getter functions; used for statistics output in game.cpp
uint GETMISS() { return cache.cache_miss; }
uint GETHITS() { return cache.cache_hits; }
uint GETACCESS() { return cache.cache_age; }
Surface* GETGRAPH() { return cache.graph; }
bool GETGRAPHFLAG() { return cache.graphFlag; }
void SETGRAPHFLAG(bool flag) { cache.graphFlag = flag; }
uint* GETACCESSTIME() { return cache.access_time; }
uint GETCURRACCESSTIME() { return cache.curr_access_time; }
uint GETTICKACCESSTIME() { return cache.tick_access_time/cache.tick_access; }
uint GETTICKACCESS() { return cache.tick_access; }
void RESETTICK() { cache.tick_access_time = 0; cache.tick_access = 0; cache.tick_peak = 0; }


// Helper functions; forward all requests to the cache
// NOTE: If Read/Write N-bit functions are not working, revert to commented functions

//uint LOADINT( uint* address ) { return cache.Read32bit( (uint)address ); }
//void STOREINT( uint* address, uint value ) { cache.Write32bit( (uint)address, value ); }
uint LOADINT( uint* address ) { return cache.ReadNbit<uint>( (uint)address ); }
void STOREINT( uint* address, uint value ) { cache.WriteNbit<uint>( (uint)address, value ); }

//float LOADFLOAT( float* a ) { uint v = LOADINT( (uint*)a ); return *(float*)&v; }
//void STOREFLOAT( float* a, float f ) { uint v = *(uint*)&f; STOREINT( (uint*)a, v ); }
float LOADFLOAT ( float* a ) { return cache.ReadNbit<float>( (uint)a ); }
void STOREFLOAT( float* a, float f ) { cache.WriteNbit<float>( (uint)a, f ); }

//vec3 LOADVEC( vec3* a ) { vec3 r; for( int i = 0; i < 4; i++ ) r.cell[i] = LOADFLOAT( (float*)a + i ); return r; }
//void STOREVEC( vec3* a, vec3 t ) { for( int i = 0; i < 4; i++ ) { float v = t.cell[i]; STOREFLOAT( (float*)a + i, v ); } }
vec3 LOADVEC( vec3* a ) { return cache.ReadNbit<vec3>( (uint)a ); }
void STOREVEC( vec3* a, vec3 t ) { cache.WriteNbit<vec3>( (uint)a, t ); }

//void* LOADPTR( void* a ) { uint v = LOADINT( (uint*)a ); return *(void**)&v; }
//void STOREPTR(void* a, void* p) { uint v = *(uint*)&p; STOREINT((uint*)a, v); }
void* LOADPTR( void* a ) { return cache.ReadNbit<void*>( (uint)a ); }
void STOREPTR( void* a, void* p ) { cache.WriteNbit<void*>( (uint)a, p ); }

// ============================================================================
// CACHE SIMULATOR IMPLEMENTATION
// ============================================================================

// Cache constructor
Cache::Cache()
{
	cache_hits = 0;
	cache_miss = 0;
	cache_age = 0;

	tick_access_time = 0;
	tick_access = 0;
	tick_peak = 0;
	
	access_start = 0;
	access_end = 0;

	count = 0;

	graph = new Surface(SCRWIDTH, SCRHEIGHT);
	graph->Clear(0);
	graphFlag = false;
}

// Cache destructor
Cache::~Cache()
{
	printf( "%f\n", dummy ); // prevent dead code elimination
}

// ============================================================================
// INPUT/OUTPUT THROUGH CACHE INTO/FROM MEMORY
// ============================================================================

// Read any N-bit value from address (Maximum size: CACHESIZE * 4 bytes)
template<typename T> T Cache::ReadNbit(uint address)
{
	__declspec(align(64)) CacheLine line;

	// Try to load line corresponding to 'address' from cache into 'line'
	if (LoadLineFromCache(address, line))
	{
		// Found line in cache; cache hit
		cache_hits++;
	}
	else
	{
		// Line not in cache; cache miss
		cache_miss++;

		// Load line from memory instead and mark as clean (value in memory = value in cache)
		LoadLineFromMem(address, line);
		line.dirty = false;

		if (STATISTICS) 
		{ 
			access_end = __rdtsc();
			curr_access_time = access_end - access_start;
			// Ignore outliers that are more likely to be caused by internal OS issues
			if (curr_access_time < 10000)
			{
				UpdateGraph();
				access_time[CACHELEVELS] = (access_time[CACHELEVELS] + (curr_access_time)) / 2;
				tick_access++;
				tick_access_time += curr_access_time;
				tick_peak = tick_peak < curr_access_time ? curr_access_time : tick_peak;
			}
		}

		// Write loaded line into cache for future use
		WriteLineToCache(address, line);
	}

	// Read a number of 4 byte segments from loaded line; determined by type of value being loaded
	uint result[sizeof(T) / 4];
	memcpy(&result, &line.data[(address & 63) >> 2], sizeof(T));

	// Return value at requested address interpreted by value type requested
	return *(T*)&result;
}

// Write any N-bit value to address (Maximum size: CACHESIZE * 4 bytes)
template<typename T> void Cache::WriteNbit(uint address, T value)
{
	__declspec(align(64)) CacheLine line;

	// Try to load line corresponding to 'address' from cache into 'line'
	if (LoadLineFromCache(address, line))
	{
		// Found line in cache; cache hit
		cache_hits++;

		// Adjust appropriate segment in loaded line to new value and rewrite to cache
		memcpy(&line.data[(address & 63) >> 2], &value, sizeof(T));
		UpdateLineInCache(address, line);
	}
	else
	{
		// Line not in cache; cache miss
		cache_miss++;

		// Load line from memory instead, adjust appropriate segment, and mark as dirty (value in memory != value in cache)
		LoadLineFromMem(address, line);
		memcpy(&line.data[(address & 63) >> 2], &value, sizeof(T));
		line.dirty = true;

		if (STATISTICS)
		{
			access_end = __rdtsc();
			curr_access_time = access_end - access_start;
			// Ignore outliers that are more likely to be caused by internal OS issues
			if (curr_access_time < 10000)
			{
				UpdateGraph();
				access_time[CACHELEVELS] = (access_time[CACHELEVELS] + (curr_access_time)) / 2;
				tick_access++;
				tick_access_time += curr_access_time;
				tick_peak = tick_peak < curr_access_time ? curr_access_time : tick_peak;
			}
		}

		// Write loaded line into cache for future use
		WriteLineToCache(address, line);
	}
}

// NOTE: Old Code now replaced by more flexible ReadNbit<>() function
// Read any 32-bit value from address
uint Cache::Read32bit(uint address)
{
	// TODO: prevent reads from RAM using a cache
	__declspec(align(64)) CacheLine line;

	ReadNbit<vec3>(address);

	if (LoadLineFromCache(address, line))
	{
		cache_hits++;
	}
	else
	{
		cache_miss++;
		// cache read miss: read data from RAM
		LoadLineFromMem(address, line);
		line.dirty = false;

		// TODO: store the data in the cache
		WriteLineToCache(address, line);
	}

	return line.data[(address & 63) >> 2];
}

// NOTE: Old Code now replaced by more flexible WriteNbit<>() function
// Write any 32-bit value to address
void Cache::Write32bit(uint address, uint value)
{
	// TODO: prevent writes to RAM using a cache
	__declspec(align(64)) CacheLine line;

	if (LoadLineFromCache(address, line))
	{
		cache_hits++;

		line.data[(address & 63) >> 2] = value;
		UpdateLineInCache(address, line);
	}
	else
	{
		cache_miss++;
		// cache write miss: write data to RAM
		LoadLineFromMem(address, line);
		line.data[(address & 63) >> 2] = value;
		line.dirty = true;

		// TODO: write to cache instead of RAM; evict to RAM if necessary
		WriteLineToCache(address, line);
	}
}

// ============================================================================
// CACHE OPERATIONS
// ============================================================================

bool Cache::LoadLineFromCache(uint address, CacheLine& line)
{
	// Cache is accessed; increment cache age.
	cache_age++;

	// Avoiding overflows.
	if (cache_age == LONG_MAX)
	{
		ResetOffsets();
		cache_age = 0;
		cache_hits = 0;
		cache_miss = 0;
	}
	
	if (STATISTICS) { access_start = __rdtsc(); }

	// Get address of the 64-byte chunk in memory and the position in those 64 bytes in which the data is stored.
	uint lineAddress = address & 0xFFFFFFC0;
	uint blockNumber = lineAddress / (CACHESIZE * sizeof(uint)); 

	for (uint c = 0; c < CACHELEVELS; c++)
	{
		// Get the row in cache that the data with the given address resides in.
		uint cacheRow = blockNumber % full_cache[c]->rows;

		// Cycle through the sets to find the data.
		for (uint i = 0; i < full_cache[c]->sets; i++)
		{
			CacheLine* current = full_cache[c]->GetCacheLine(cacheRow, i);

			// If the data is found.
			if (current->valid && (lineAddress == (current->tag & 0xFFFFFFC0)))
			{
				memcpy(line.data, current->data, 64);

				if ((policy == POLICY::LRU) || (policy == POLICY::MRU))
				{
					// Update the offset variable of the cache line to the age of the cache. This way, the age of the line can be found by doing cache_age - current->offset.
					current->offset = cache_age;
				}
				else if (policy == POLICY::LFU)
				{
					// Avoiding overflows.
					if (current->hits < ULONG_MAX)
					{
						current->hits++;
					}
				}

				if (STATISTICS)
				{
					access_end = __rdtsc();
					curr_access_time = access_end - access_start;
					// Ignore outliers that are more likely to be caused by internal OS issues
					if (curr_access_time < 10000)
					{
						UpdateGraph();
						access_time[c] = (access_time[c] + (curr_access_time)) / 2;
						tick_access++;
						tick_access_time += curr_access_time;
						tick_peak = tick_peak < curr_access_time ? curr_access_time : tick_peak;
					}
				}
				return true;
			}
		}
	}

	return false;
}

void Cache::WriteLineToCache(uint address, CacheLine& line)
{
	if (STATISTICS) { access_start = __rdtsc(); }

	// Get address of the 64-byte chunk in memory and the position in those 64 bytes in which the data is stored.
	uint lineAddress = address & 0xFFFFFFC0;
	uint blockNumber = lineAddress / (CACHESIZE * sizeof(uint));

	for (uint c = 0; c < CACHELEVELS; c++)
	{
		// Get the row in cache that the data with the given address should reside in.
		uint cacheRow = blockNumber % full_cache[c]->rows;

		// Cycle through the sets to find the data.
		for (uint i = 0; i < full_cache[c]->sets; i++)
		{
			CacheLine* current = full_cache[c]->GetCacheLine(cacheRow, i);

			// If a spot is open.
			if (!current->valid)
			{
				// Occupy the spot with the new data.
				int setOffset = 6 - log2(full_cache[c]->sets);
				memcpy(current->data, line.data, 64);
				current->tag = lineAddress + (i << setOffset);
				current->valid = true;
				current->dirty = line.dirty;
				current->offset = this->cache_age;

				if (STATISTICS)
				{
					access_end = __rdtsc();
					curr_access_time = access_end - access_start;
					// Ignore outliers that are more likely to be caused by internal OS issues
					if (curr_access_time < 10000)
					{
						UpdateGraph();
						access_time[c] = (access_time[c] + (curr_access_time)) / 2;
						tick_access++;
						tick_access_time += curr_access_time;
						tick_peak = tick_peak < curr_access_time ? curr_access_time : tick_peak;
					}
				}
				return;
			}
		}
	}

	// We only reach this point if no spot was found. Therefore, what is already in the cache must be evicted.
	Evict(address, line);
}

void Cache::UpdateLineInCache(uint address, CacheLine& line)
{
	if (STATISTICS) { access_start = __rdtsc(); }

	// Get address of the 64-byte chunk in memory and the position in those 64 bytes in which the data is stored.
	uint lineAddress = address & 0xFFFFFFC0;
	uint blockNumber = lineAddress / (CACHESIZE * sizeof(uint));

	for (uint c = 0; c < CACHELEVELS; c++)
	{
		// Get the row in cache that the data with the given address resides in.
		uint cacheRow = blockNumber % full_cache[c]->rows;

		// Cycle through the sets to find the data.
		for (uint i = 0; i < full_cache[c]->sets; i++)
		{
			CacheLine* current = full_cache[c]->GetCacheLine(cacheRow, i);

			// If the data is found.
			if (lineAddress == (current->tag & 0xFFFFFFC0))
			{
				// Update the cache line with the new data.
				memcpy(current->data, line.data, 64);
				current->dirty = true;
				current->offset = this->cache_age;

				if (STATISTICS)
				{
					access_end = __rdtsc();
					curr_access_time = access_end - access_start;
					// Ignore outliers that are more likely to be caused by internal OS issues
					if (curr_access_time < 10000)
					{
						UpdateGraph();
						access_time[c] = (access_time[c] + (curr_access_time)) / 2;
						tick_access++;
						tick_access_time += curr_access_time;
						tick_peak = tick_peak < curr_access_time ? curr_access_time : tick_peak;
					}
				}
				return;
			}
		}
	}
}

void Cache::Evict(uint address, CacheLine line)
{
	// Variable to store the set that needs to be evicted.
	uint replaceSet = 0;

	// Cycle through all the levels. What gets evicted from one level in cache goes to the next level. Only the data in the last level of cache gets evicted to memory.
	for (uint c = 0; c < CACHELEVELS; c++)
	{
		// Get address of the 64-byte chunk in memory, the position in those 64 bytes in which the data is stored, and the row in cache that the data with the given address should reside in.
		uint lineAddress = address & 0xFFFFFFC0;
		uint blockNumber = lineAddress / (CACHESIZE * sizeof(uint));
		uint cacheRow = blockNumber % full_cache[c]->rows;

		switch (policy)
		{
			// Least Recently Used
			case POLICY::LRU:
			{
				uint maxAge = 0;

				// Choose the oldest cache line to evict.
				for (uint i = 0; i < full_cache[c]->sets; i++)
				{
					CacheLine* current = full_cache[c]->GetCacheLine(cacheRow, i);

					if ((cache_age - current->offset >= maxAge))
					{
						maxAge = cache_age - current->offset;
						replaceSet = i;
					}
				}
			}
			break;

			// Most Recently Used
			case POLICY::MRU:
			{
				uint minAge = UINT_MAX;

				// Choose the youngest cache line to evict.
				for (uint i = 0; i < full_cache[c]->sets; i++)
				{
					CacheLine* current = full_cache[c]->GetCacheLine(cacheRow, i);

					if ((cache_age - current->offset) <= minAge)
					{
						minAge = cache_age - current->offset;
						replaceSet = i;
					}
				}
			}
			break;

			// Random
			case POLICY::RANDOM:
			{
				// Seed the random number generator and generate a random set number to evict.
				srand(time(NULL));
				replaceSet = rand() % full_cache[c]->sets;
			}
			break;

			// Least Frequently Used
			case POLICY::LFU:
			{
				uint minHits = UINT_MAX;

				// Choose the line with the least hits to evict.
				for (uint i = 0; i < full_cache[c]->sets; i++)
				{
					CacheLine* current = full_cache[c]->GetCacheLine(cacheRow, i);

					if (current->hits < minHits)
					{
						minHits = current->hits;
						replaceSet = i;
					}
				}
			}
			break;
		}

		CacheLine* replace = full_cache[c]->GetCacheLine(cacheRow, replaceSet);

		// If we've reached the last level in cache, we write to memory.
		if (c == CACHELEVELS - 1)
		{
			if (replace->dirty)
			{
				WriteLineToMem(replace->tag, *replace);
			}
		}

		// Store temporary variables to keep the data that is to be evicted.
		uint tempAddress = replace->tag;
		CacheLine tempLine = *replace;
		int setOffset = 6 - log2(full_cache[c]->sets);

		// Replace the old data with the new data.
		ResetLine(*replace);
		memcpy(replace->data, line.data, 64);
		replace->tag = (address & 0xFFFFFFC0) + (replaceSet << setOffset);
		replace->valid = true;
		replace->dirty = line.dirty;
		replace->offset = this->cache_age;

		// Store the old data in the variables that will be used in the next iteration.
		address = tempAddress;
		line = tempLine;
	}
}

void Cache::ResetLine(CacheLine& line)
{
	// Restore a line to its default values.
	line.offset = this->cache_age;
	line.dirty = false;
	line.hits = 0;
	line.valid = false;
}

void Cache::ResetOffsets()
{
	// We only reach this method if cache_age has hit its LONG_MAX value. It cannot go any higher so it must be reset to 0.
	// Therefore, go through all the cache levels and update the age offsets to reflect this change such that cache_age - current->offset will still return the correct age of the cache line.
	for (uint c = 0; c < CACHELEVELS; c++)
	{
		for (uint i = 0; i < full_cache[c]->rows; i++)
		{
			for (uint j = 0; j < full_cache[c]->sets; j++)
			{
				CacheLine* current = full_cache[c]->GetCacheLine(i, j);

				if (current->valid)
				{
					current->offset = current->offset - cache_age;
				}
			}
		}
	}
}

// ============================================================================
// CACHE SIMULATOR LOW LEVEL MEMORY ACCESS WITH LATENCY SIMULATION
// ============================================================================

uint* RAM = (uint*)MALLOC64( 20 * 1024 * 1024 ); // simulated RAM

// Load a cache line from memory; simulate RAM latency
void Cache::LoadLineFromMem( uint address, CacheLine& line )
{
    uint lineAddress = address & 0xFFFFFFC0; // set last six bit to 0
    memcpy( line.data, (void*)lineAddress, 64 ); // fetch 64 bytes into line
    DELAY;
}

// Write a cache line to memory; simulate RAM latency
void Cache::WriteLineToMem( uint address, CacheLine& line )
{
    uint lineAddress = address & 0xFFFFFFC0; // set last six bit to 0
    memcpy( (void*)lineAddress, line.data, 64 ); // fetch 64 bytes into line
    DELAY;
}

// ============================================================================
// STATISTICS GRAPH
// ============================================================================

void Cache::UpdateGraph()
{
	if (graphFlag)
	{
		if (tick_access == 0) graph->Clear(0);
		graph->Line(tick_access, SCRHEIGHT - 1, tick_access, SCRHEIGHT - curr_access_time/2, 0x0064a8);
	}
}